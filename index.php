  
  <!DOCTYPE html>
<?php
  include 'connect.php';  
  require 'header.php';
?>
  <div class="container">
    <div class="section">

      <div class="slider">
          <ul class="slides">
            <li>
              <img src="utama/img/1.jpg"> <!-- random image -->
              <div class="caption center-align">
                <h3 class="light cyan-text text-lighten-5">CRACKED:</h3>
                <h5 class="light cyan-text text-lighten-5">Running out of water</h5>
              </div>
            </li>
            <li>
              <img src="utama/img/2.jpg"> <!-- random image -->
              <div class="caption left-align">
                <h3 class="light cyan-text text-lighten-5">Stop Drought!</h3>
                <h5 class="light cyan-text text-lighten-5">Until the rain falls</h5>
              </div>
            </li>
            <li>
              <img src="utama/img/3.jpg"> <!-- random image -->
              <div class="caption right-align">
                <h3 class="light indigo-text text-darken-4">Help us!</h3>
                <h5 class="light indigo-text text-darken-4">We are in Drought</h5>
              </div>
            </li>
            <li>
              <img src="utama/img/4.jpg"> <!-- random image -->
              <div class="caption center-align">
                <h3 class="light indigo-text text-darken-4">Stop Drought!</h3>
                <h5 class="light indigo-text text-darken-4">Hack the drought</h5>
              </div>
            </li>
          </ul>
        </div>
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">AirKita</h5>

            <p class="hoverable" ><img src="utama/img/text1.png"></p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">Fitur</h5>

            <p class="hoverable" ><img src="utama/img/text2.png"></p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
            <h5 class="center">Cara Kerja</h5>

            <p class="hoverable" ><img src="utama/img/text3.png"></p>
          </div>
        </div>
      </div>

    </div>
    <br><br>

    <div class="section">

    </div>
  </div>

  <?php
  require 'footer.php';
?>