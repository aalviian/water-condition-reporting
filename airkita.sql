-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2015 at 02:11 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airkita`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `passadmin` varchar(45) NOT NULL,
  `namainstansi` varchar(45) NOT NULL,
  `usernameadmin` varchar(45) NOT NULL,
  `emailadmin` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idadmin`, `passadmin`, `namainstansi`, `usernameadmin`, `emailadmin`) VALUES
(1, '123', 'Kepala Dinas Binamarga dan Pengairan', 'binamarga', 'binamarga@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE `policy` (
  `idpolicy` int(11) NOT NULL,
  `idreport` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `postTitle` varchar(200) NOT NULL,
  `postCont` longtext CHARACTER SET armscii8,
  `postDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy`
--

INSERT INTO `policy` (`idpolicy`, `idreport`, `iduser`, `postTitle`, `postCont`, `postDate`) VALUES
(8, 34, 42, 'Harap bersabar sukaraja!', 'Harap Tenang ya :)', '2015-12-28 17:27:29'),
(9, 33, 42, 'Sabar Warga Babakan!', 'Yayayaya', '2015-12-28 18:17:19');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `idreport` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `kotakab` varchar(25) NOT NULL,
  `kec` varchar(20) NOT NULL,
  `kel` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `lat` varchar(20) NOT NULL,
  `long` varchar(20) NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`idreport`, `iduser`, `kotakab`, `kec`, `kel`, `alamat`, `foto`, `timestamp`, `lat`, `long`, `deskripsi`) VALUES
(33, 42, 'Kabupaten', 'Dramaga', 'Babakan ', 'Jalan Babakan Lebak', 'user/wilayah/20887_18522_OKE-FOTO-A-ARIESANT-20-HEKT.jpg', '2015-12-28 15:53:23', '-6.569714406704295', '106.7400848865509', 'Tolong selamatkan kami dari kekeringan'),
(34, 42, 'Kota', 'Bogor Utara', 'Sukaraja', 'Jalan Pegagan ', 'user/wilayah/150730140355-kota-tasikmalaya-tetapkan-siaga-kekeringan.jpg', '2015-12-28 15:54:04', '-6.543792542696971', '106.81836247444153', 'Kami sedang kesusahan untuk mencari air bersih');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `nameUser` text CHARACTER SET armscii8 NOT NULL,
  `emailUser` varchar(100) NOT NULL,
  `passUser` varchar(45) NOT NULL,
  `otherpass` varchar(40) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `dateUser` text NOT NULL,
  `alamatUser` text NOT NULL,
  `phoneUser` text NOT NULL,
  `photoUser` varchar(100) CHARACTER SET armscii8 NOT NULL,
  `level` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `nameUser`, `emailUser`, `passUser`, `otherpass`, `hash`, `dateUser`, `alamatUser`, `phoneUser`, `photoUser`, `level`) VALUES
(42, 'Alvian', 'aalviian@gmail.com', '123', 'e10adc3949ba59abbe56e057f20f883e', 'cacf3dac440f99a1be9ef84da6c639c4', '6 September, 1995', 'Jalan Pulosari 3', '0879562752734', 'user/images/1.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indexes for table `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`idpolicy`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`idreport`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `policy`
--
ALTER TABLE `policy`
  MODIFY `idpolicy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `idreport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
