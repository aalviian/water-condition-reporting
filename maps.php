<?php
include 'connect.php';
    if($_SESSION['status'] != "user")
    {?>
        <script type="text/javascript">window.location.assign("masuk.php")</script>
    <?php
    }

    if($_SESSION['status'] == "user") {

    $id = $_SESSION['status'];
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Maps</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="user/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="user/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="user/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="user/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


  <nav class="light-blue lighten-1" role="navigation">
      <div class="nav-wrapper">
        <a href="home.php" class="brand-logo">   Airkita</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="home.php">Beranda</a></li>
            <li><a href="profile.php">Profil</a></li>
            <li class="active"><a href="maps.php">Peta</a></li>
            <li><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="home.php">Beranda</a></li>
            <li><a href="profile.php">Profil</a></li>
            <li class="active"><a href="maps.php">Peta</a></li>
            <li><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
      </div>
    </nav>

	<div class="card-panel grey lighten-2">
    <div class="container">
      <div class="row">
      		<h4>Peta Wilayah Kekeringan</h4>
              <br>
      		<div class="card-panel teal hoverable">
             <div id="map"></div>
                <script>

                    function initMap() {

                      var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 11,
                        center: {lat: -6.596553, lng:  106.795665}
                      });

                        <?php 
                        $query=mysqli_query($conn, "SELECT * FROM report");
                        while ($idreport=mysqli_fetch_assoc($query)) { ?>

                          var myLatLng = {lat:<?php echo $idreport['lat']?>, lng:<?php echo $idreport['long']?>};

                          var marker = new google.maps.Marker({
                            position: myLatLng, 
                            map: map,
                            title: 'Drought Report!'
                          });

                          var infowindow = new google.maps.InfoWindow({
                          content: 
    					  '<?php echo $idreport['kel']?>' + '<br>Latitude: ' + <?php echo $idreport['lat']?> + '<br>Longitude: ' + <?php echo $idreport['long']?> + '<br>Alamat lengkap: <?php echo $idreport['alamat']?>' + '<br>Waktu post: <?php echo $idreport['timestamp']?>'
                          });
    					  
    						infowindow.open(map, marker);

                      <?php } ?>
                     }

                  </script>

                  <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA8aEnIxREfHqK8FTd3LOFQeP1tIlxI4U&signed_in=true&callback=initMap" type="text/javascript"></script>

	     	 </div>
      </div>
	 </div>
	</div>

    <script src="user/material.min.js"></script>
    <script src="utama/js/jquery-2.1.4.min.js"></script>
    <script src="utama/js/materialize.min.js"></script>
    <script src="utama/js/init.js"></script>
    <script src="utama/js/jquery.chained.min.js"></script>
    <script>
         $(".button-collapse").sideNav();
        $('.fixed-action-btn').openFAB();
        $('.fixed-action-btn').closeFAB();
    </script>

    <?php } ?>
  </body>
  </html>
<?php } ?>