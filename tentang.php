<?php
  include 'connect.php';  
  require 'header.php';
?>
  <div class="container">
    <div class="section">
      <div class="row center">
        <h4>Tentang kita</h4>

        <div class="section">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="utama/img/gambar1.jpg">
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Airkita's Team<i class="material-icons right">more_vert</i></span>
              <p><a href="#team">Team Work</a></p>
            </div>
            <div class="card-reveal span.card-title">
              <div class="activator">
              <span class="card-title grey-text text-darken-4">Airkita's Team<i class="material-icons right">close</i></span>
              <p>Kami membuat sistem bertujuan untuk memudahkan warga Bogor dalam menyampaikan masukkannya terkait pemasokkan air di PDAM seluruh wilayah kota dan kabupaten Bogor. Pada dasarnya air merupakan sumber utama dalam kebutuhan sehari-hari</p>
              <p>Sistem dibuat oleh 3 mahasiswa dari Institut Pertanian Bogor</p>
              </div>
            </div>
          </div>
        </div>

        <section id="team" class="section">
        <div class = "container">
        <h5>Team Work</h5>
        </div>
            <div class="col s4">
              <p class="z-depth-5">
               <img class="materialboxed " data-caption="A picture of some deer and tons of trees" width="250" src="utama/img/profil1.jpg">
               Alvian
              </p>
            </div>
            <div class="col s4">
              <p class="z-depth-5">
               <img class="materialboxed" data-caption="A picture of some deer and tons of trees" width="250" src="utama/img/profil1.jpg">
               Gopur
              </p>
            </div>
            <div class="col s4">
              <p class="z-depth-5">
               <img class="materialboxed" data-caption="A picture of some deer and tons of trees" width="250" src="utama/img/profil1.jpg">
               Luthfiyah
              </p>
            </div>
        </section>

      </div>
    </div>
  </div>

  <?php
  require 'footer.php';
?>