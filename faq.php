<?php
  include 'connect.php';  
  require 'header.php';
?>
  <div class="container">
    <div class="section">
      <div class="row center">
        <h4>Frequently Asked Question</h4>

        <div class="section">
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header"><i class="material-icons">live_help</i>Apakah saya harus daftar terlebih dahulu?</div>
                <div class="collapsible-body"><p>Iya, memang diwajibkan untuk daftar terlebih dahulu</p></div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">live_help</i>Bagaimana kalau orang selain bogor daftar dan menggunakan aplikasi ini?</div>
                <div class="collapsible-body"><p>Jika daftar saja tidak apa-apa, tapi sistem kami mengabaikan jika ada laporan diluar Bogor</p></div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">live_help</i>Jika saya mendaftar dengan email selain gmail bagaimana?</div>
                <div class="collapsible-body"><p>Tidak diwajibkan untuk memakai Gmail, hanya saja diusahakan untuk memakainya</p></div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">live_help</i>Jika saya mendaftar dengan email selain gmail bagaimana?</div>
                <div class="collapsible-body"><p>Tidak diwajibkan untuk memakai Gmail, hanya saja diusahakan untuk memakainya</p></div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">live_help</i>Jika saya mendaftar dengan email selain gmail bagaimana?</div>
                <div class="collapsible-body"><p>Tidak diwajibkan untuk memakai Gmail, hanya saja diusahakan untuk memakainya</p></div>
              </li>
            </ul>
        </div>



      </div>
    </div>
  </div>

  <?php
  require 'footer.php';
?>