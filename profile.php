<?php
include 'connect.php';
    if($_SESSION['status'] != "user")
    {?>
        <script type="text/javascript">window.location.assign("masuk.php")</script>
    <?php
    }

    if(isset($_SESSION['status'])) {
    $id = $_SESSION['status'];
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Profile</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="user/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="user/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="user/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="user/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  </head>
  <body>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


  <nav class="light-blue lighten-1" role="navigation">
      <div class="nav-wrapper">
        <a href="home.php" class="brand-logo">   Airkita</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="home.php">Beranda</a></li>
            <li class="active"><a href="profile.php">Profil</a></li>
            <li><a href="maps.php">Peta</a></li>
            <li><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="home.php">Beranda</a></li>
            <li class="active"><a href="profile.php">Profil</a></li>
            <li><a href="maps.php">Peta</a></li>
            <li><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
      </div>
    </nav>
<div class="card-panel grey lighten-2">
	<div class="container">
		<div class="row">

		  <h4>Profil : <?php echo $result['nameUser']?>  </h4>
		  <BR>
			<div class="card blue-grey lighten-1">
			  <div class="card-content white-text">
				<div class="col s12">
				  <ul class="tabs" class="hoverable">
					<li class="tab col s3"><a class="active" href="#test1">Umum</a></li>
					<li class="tab col s3"><a href="#test2">Kontak</a></li>
					<li class="tab col s3"><a href="#test3">Privasi</a></li>
					<li class="tab col s3"><a href="#test4">Foto</a></li>
				  </ul>
				</div>
				<div id="test1" class="col s12">
				  <div class="col s4">
					<img src="<?php echo $result['photoUser']?>" class="circle responsive-img" width="250" height="250" alt="">
				  </div>
				  <div class="col s8">
					<table>
					  <thead>
					  <form action="changegeneral.php">
						<tr>
							<th data-field="id">Informasi Umum</th>
							<th data-field="name"><button class="btn waves-effect waves-light" type="submit" name="action">Ubah</button></th>
						</tr>
					  </form>
					  </thead>
					  <tbody>
						<tr>
						  <td>Nama Lengkap:</td>
						  <td><?php echo $result['nameUser']?></td>
						</tr>
						<tr>
						  <td>Tanggal Lahir:</td>
						  <td><?php echo $result['dateUser']?></td>
						</tr>
						<tr>
						  <td>Alamat Lengkap:</td>
						  <td><?php echo $result['alamatUser']?></td>
						</tr>						
					  </tbody>
					</table>
				  </div>
				</div>
				<div id="test2" class="col s12">
				  <div class="col s4">
					<img src="<?php echo $result['photoUser']?>" class="circle responsive-img" width="250" height="250">
				  </div>
				  <div class="col s8">
					<table>
					  <thead>
					  <form action="changecontact.php">
						<tr>
							<th data-field="id">Informasi Kontak</th>
							<th data-field="name"><button class="btn waves-effect waves-light" type="submit" name="action">Ubah</button></th>
						</tr>
					  </form>
					  </thead>
					  <tbody>
						<tr>
						  <td>Email:</td>
						  <td><?php echo $result['emailUser']?></td>
						</tr>
						<tr>
						  <td>No Handphone:</td>
						  <td><?php echo $result['phoneUser']?></td>
						</tr>
					  </tbody>
					</table>
				  </div>
				</div>
				<div id="test3" class="col s12">
				  <div class="col s4">
					<img src="<?php echo $result['photoUser']?>" class="circle responsive-img" width="250" height="250">
				  </div>
				  <div class="col s8">
					<table>
					  <thead>
						<tr>
							<form action="changepassword.php">
							<th data-field="id">Informasi Privasi</th>
							<th data-field="name"><button class="btn waves-effect waves-light" type="submit" name="action">Ubah</button></th>
							</form>
						</tr>
					  </thead>
					  <tbody>
						<tr>
						  <td>Password:</td>
						  <td> <input disabled value="<?php echo $result['passUser']?>" id="disabled" type="password" class="validate"></td>
						</tr>
					  </tbody>
					</table>
				  </div>
				</div>
				<div id="test4" class="col s12">
				  <div class="col s4">
					<img src="<?php echo $result['photoUser']?>" class="circle responsive-img" width="250" height="250">
				  </div>
				  <div class="col s8">
					<table>
					  <thead>
						<tr>
							<th data-field="id">Foto Profil</th>
							<th data-field="name"></th>
						</tr>
					  </thead>
					  <tbody>
						<tr>
						  <td>Kamu dapat ubah fotomu</td>
						</tr>
						<tr>
						  <td>
							<form action="changepicture.php"  name="uploader" method="post" enctype="multipart/form-data">
							  <i class="material-icons prefix"></i>
								  <div class="file-field input-field">
									<div class="btn">
									  <span>File</span>
									  <input type="file" name="photo">
									</div>
									<div class="file-path-wrapper">
									  <input class="file-path validate" type="text" name="photo" placeholder="Upload fotomu">
									</div>
								  </div>     
							  <button class="btn waves-effect waves-light" type="submit" name="action">Ubah</button>
							</form>
						  </td>
						</tr>
					  </tbody>
					</table>
				  </div>
				</div>


			  </div>
			</div>
		</div>
		<div class="card-panel blue-grey darken-4">
			<div class="card-content white-text">
				Riwayat Laporan
			</div>
		</div>
		<?php 
			$query2=mysqli_query($conn, "SELECT * FROM report WHERE idUser = '$id' ORDER BY timestamp desc");
			while ($result2=mysqli_fetch_assoc($query2)) { ?>
		<br>
		<div class="container">
			<div class="row">
			<div class="section">
              <div class="card">
                <p>Posted on <?php echo $result2['timestamp']?>&nbsp &nbsp &nbsp &nbsp <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Hapus</a>
			  <div id="modal1" class="modal">
				<div class="modal-content">
				  <h4>Pemberitahuan</h4>
				  <p>Apakah anda yakin untuk menghapus report ini?</p>
				</div>
				<div class="modal-footer">
				  <a href="profile.php" class=" modal-action modal-close waves-effect waves-green btn-flat">Batal</a>
				  <a href="hapusreport.php?idreport=<?php echo $result2['idreport'];?>" class="modal-action modal-close waves-effect waves-green btn-flat">Hapus</a>
				</div>
			  </div></p>
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="<?php echo $result2['foto']?>">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Daerah : <?php echo $result2['kec']?><i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal span.card-title">
                  <div class="activator">
                  <span class="card-title grey-text text-darken-4">Daerah : <?php echo $result2['kec']?><i class="material-icons right">close</i></span>
                  <p><?php echo $result2['deskripsi']?></p>
                  </div>
                </div>
              </div>
            </div>
			</div>
		</div>
		<?php } ?>
		</div>
	</div>
</div>

    <script src="user/material.min.js"></script>
    <script src="utama/js/jquery-2.1.4.min.js"></script>
    <script src="utama/js/materialize.min.js"></script>
    <script src="utama/js/init.js"></script>
    <script src="utama/js/jquery.chained.min.js"></script>
    <script>
			$(document).ready(function(){
		$('.modal-trigger').leanModal();
		});
         $(".button-collapse").sideNav();
        $('.fixed-action-btn').openFAB();
        $('.fixed-action-btn').closeFAB();

          
    </script>

    <?php } ?>
  </body>
  </html>
  <?php } ?>