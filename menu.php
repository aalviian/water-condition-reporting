<?php
  include 'connect.php';
  if($_SESSION['id']=="user"){
      ?>
      <script language="javascript">alert("Anda harus login terlebih dahulu");</script>
      <script>document.location.href='masuk.php';</script>
  <?php
  }
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="user/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="user/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="user/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="user/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/ripples.min.css" rel="stylesheet">
    <link href="utama/css/animate.min.css" rel="stylesheet">
  </head>
  <body>


  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>

<nav>
  <ul id="slide-out" class="side-nav">
    <li class="active"><a href="home.php">Timeline</a></li>
    <li><a href="profile.php">Profile</a></li>
    <li><a href="post.php">Post</a></li>
    <li><a href="maps.php">Maps</a></li>
    <li><a href="inbox.php">Inbox</a></li>
    <li><a href="faquser.php">FAQ</a></li>
    <li><a href="logoutproses.php">Logout</a></li>
  </ul>
  <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="mdi-navigation-menu"></i></a>
</nav>
  

    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
          <a class="btn-floating btn-large red">
            <i class="large material-icons">mode_edit</i>
          </a>
          <ul>
            <li><a  href="post.php" class="btn-floating red"><i class="material-icons">note_add</i></i></a></li>
            <li><a href="maps.php" class="btn-floating yellow darken-1"><i class="material-icons">business</i></a></li>
            <li><a class="btn-floating green"><i class="material-icons">perm_identity</i></a></li>
            <li><a class="btn-floating blue"><i class="material-icons">payment</i></a></li>
          </ul>
        </div>
    </main>


    <script src="user/material.min.js"></script>
    <script src="utama/js/jquery-2.1.4.min.js"></script>
    <script src="utama/js/materialize.min.js"></script>
    <script src="utama/js/init.js"></script>
    <script src="utama/js/jquery.chained.min.js"></script>
  <script>

$('.button-collapse').sideNav('hide');
// Hide sideNav
$('.button-collapse').sideNav('show');
// Show sideNav

</script>

    <?php } ?>
  </body>
  </html>