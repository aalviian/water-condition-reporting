<?php
include 'connect.php';
    if($_SESSION['status'] != "user")
    {?>
        <script type="text/javascript">window.location.assign("masuk.php")</script>
    <?php
    }

    if($_SESSION['status'] == "user") {

    $id = $_SESSION['status'];
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inbox</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="user/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="user/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="user/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="user/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/responsive.css" rel="stylesheet">
    <link href="utama/css/animate.min.css" rel="stylesheet">
  </head>
  <body>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


  <nav class="light-blue lighten-1" role="navigation">
      <div class="nav-wrapper">
        <a href="home.php" class="brand-logo">   Airkita</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="home.php">Beranda</a></li>
            <li><a href="profile.php">Profil</a></li>
            <li><a href="maps.php">Peta</a></li>
            <li class="active"><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="home.php">Beranda</a></li>
            <li><a href="profile.php">Profil</a></li>
            <li><a href="maps.php">Peta</a></li>
            <li class="active"><a href="inbox.php">Kotak Masuk</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Keluar</a></li>
        </ul>
      </div>
    </nav>

	<div class="card-panel grey lighten-2">
	
    <div class="container">
      <div class="row">
        <h4>Kotak Masuk</h4>
		  <?php
			  $queri = mysqli_query($conn,"SELECT * FROM policy where iduser='$id' ORDER BY postDate desc");
			  while ($policy = mysqli_fetch_assoc($queri)) {
		  ?>
        <div class="col s12 m6">
          <div class="card blue-grey darken-1 hoverable">
            <div class="card-content white-text">
              <span class="card-title">Laporan tanggal <?php echo $policy['postDate']; ?></span>
              <p><h4><?php echo $policy['postTitle']; ?></h4></p>
              <p><?php echo $policy['postCont']; ?></p>
            </div>
            <div class="card-action">
              <a href="home.php">Lihat timeline</a>
              <a href="post.php">kirim laporan</a>
            </div>
          </div>
        </div>
		<?php } ?>
      </div>
    </div>
	
	</div>
	
    <script src="user/material.min.js"></script>
    <script src="utama/js/jquery-2.1.4.min.js"></script>
    <script src="utama/js/materialize.min.js"></script>
    <script src="utama/js/init.js"></script>
    <script src="utama/js/count-to.js"></script>   
    <script src="utama/js/jquery.inview.min.js"></script>     
    <script src="utama/js/main.js"></script>
    <script src="utama/js/classie.js"></script>
    <script src="utama/js/wow.js"></script>
    <script src="utama/js/smooth-on-scroll.js"></script>
    <script src="utama/js/smooth-scroll.js"></script>
    <script src="utama/js/jquery.chained.min.js"></script>
    <script>
         $(".button-collapse").sideNav();
        $('.fixed-action-btn').openFAB();
        $('.fixed-action-btn').closeFAB();
    </script>

    <?php } ?>
  </body>
  </html>

  <?php } ?>