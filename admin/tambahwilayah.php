<?php
  include 'connect.php';
  if($_SESSION['id']=="user"){
      ?>
      <script language="javascript">alert("Anda harus login terlebih dahulu");</script>
      <script>document.location.href='loginadmin.php';</script>
  <?php
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home | Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">


    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>
<body>

	<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hello, <?php echo $result['nameUser'] ?> !</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="divider"></li>
                <li><a href="logoutproses.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="postreport.php"><i class="fa fa-edit fa-fw"></i>Post Report</a>
                        </li>
                        <li>
                            <a href="tambahwilayah.php"><i class="fa fa-bar-chart-o fa-fw"></i>Tambah Wilayah</a>
                        </li>
                        <li>
                            <a href="maps.php"><i class="fa fa-table fa-fw"></i>Maps</a>
                        </li>
                        <li>
                            <a href="daftarkeluhan.php"><i class="fa fa-edit fa-fw"></i>Daftar Keluhan</a>
                        </li>
                        <li>
                            <a href="listuser.php"><i class="fa fa-edit fa-fw"></i>Daftar User</a>
                        </li>
                        <li>
                            <a href="listwilayah.php"><i class="fa fa-edit fa-fw"></i>Daftar Wilayah</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


	<div id="page-wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <h1 class="page-header">Post Report</h1>
	        </div>
	    </div>

	   <div class="row">
				<style type='text/css'>
				.eventtext {width:100%; margin-top:20px; font:10pt Arial; text-align:left; 
					line-height:25px; background-color:#EDF4F8;padding:5px; border:1px dashed #C2DAE7;}
				#mapa {width:100%; height:340px; border:5px solid #DEEBF2;}
				ul {font:10pt arial; margin-left:25px; padding:0px;}
				li {margin-left:0px; padding:5px; list-style-type:decimal;}
				.small {font:9pt arial; color:gray; padding:2px; }
				</style>

				<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyAA8aEnIxREfHqK8FTd3LOFQeP1tIlxI4U" type="text/javascript"></script>



				<div id="mapa"></div>
				<div class="eventtext">
				<div>Lattitude : <span id="latspan"></span></div>
				<div>Longitude: <span id="lngspan"></span></div>
				<div>Lat Long  : <span id="latlong"></span></div>
				<div>Lat & Long on click: 
				  <input type="text" id="latlongclicked" style="width:300px; border:1px inset gray;"></span></div>
				</div>

				<br />


				<script type="text/javascript">
					if (GBrowserIsCompatible()) 
					{
						map = new GMap2(document.getElementById("mapa"));
						map.addControl(new GLargeMapControl());
						map.addControl(new GMapTypeControl(3));	
						map.setCenter( new GLatLng(-6.557040, 106.738082), 11,0);
						
						GEvent.addListener(map,'mousemove',function(point)
						{
							document.getElementById('latspan').innerHTML = point.lat()
							document.getElementById('lngspan').innerHTML = point.lng()
							document.getElementById('latlong').innerHTML = point.lat() + ', ' + point.lng()						
						});
						
						GEvent.addListener(map,'click',function(overlay,point)
						{
							document.getElementById('latlongclicked').value = point.lat() + ', ' + point.lng()
						});
					}
				</script>

				<!-- by line -->

					<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=124948124275250";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>

		</div>

		<div class="row">
			<div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            
                    </div>
                    <div class="panel-body">
						<div class="row">
							<div class="col-lg-8">
								<form role="form" action="tambahwilayahproses.php" method="POST">
									<div class="form-group">
										<label>Choose Kota/Kab</label>
										<select class="form-control" id="color">                
											<option value=""> choose options </option>                
											<option value="Kota">Kota</option>
											<option value="Kabupaten">Kabupaten</option>
										</select>
									</div>									
									<div class="form-group">
										<label>Kecamatan</label>
										<input class="form-control" name="kec">
										<p class="help-block">Sesuai kecamatan yang ada di kota atau kabupaten Bogor</p>
									</div>
									<div class="form-group">
										<label>Kelurahan</label>
										<input class="form-control" name="kel">
										<p class="help-block">Sesuai kelurahan yang ada di kecamatan baik kota ataupun kab Bogor</p>
									</div>
									<div class="form-group">
										<label>Latitude</label>
										<input class="form-control" name="lat">
										<p class="help-block">Sesuai dengan kelurahan yang di inginkan</p>
									</div>
									<div class="form-group">
										<label>Longitude</label>
										<input class="form-control" name="long">
										<p class="help-block">Sesuai dengan kelurahan yang di inginkan</p>
									</div>
									<button type="submit" class="btn btn-default">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</form>
							</div>
						</div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	</div>
		
				<!-- end div main -->

				<script type="text/javascript">var switchTo5x=true;</script>
				<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
				<script type="text/javascript">stLight.options({publisher: "d5d04ed8-04d1-4c39-9336-46d4c4ce9d65", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

				<!-- Google Analytics -->

				<script type="text/javascript">
				var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
				document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
				</script>
				<script type="text/javascript">
				var pageTracker = _gat._getTracker("UA-5776689-2");
				pageTracker._initData();
				pageTracker._trackPageview();
				</script>

				<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582AaN6h071sG%2bzK1Qe56RQau%2fBAIH9o0zEwzSZBW7JIyzZlB0W%2fb8n9aBwaU%2bOg%2b%2b3qIbg1lMV9RdyJux%2f8gxM6iXG84JPQL8ZuHP1GILfQEMVExM0%2bEl2kNJ845jbbTLFhLjF15oBUErMLOu0HGfcLKUhI8DJVcRY77qLWudKvzpQQFddkjHSpDODw7JYFLrl79syA25IWsmN0D59WLXgJujFAzZpZGzUGfRiZ1YlQv8Ad6WfeWoGu0BZfNw43bJdNe0sIACJSv2pv9DVdwczPeKmOcPkpqMkiA0kelV3k4ut3khd5tXyObgCbYCApFcGdEhcc4LBQXp6SX8SSHSGz1vlOdj4Z1KHDjozoGtHgjJSmuGXDD4A5AbYgRKj33wisR37cjKPToX6jfPZzmgurPAcmhXsjFrPZOJ3nncWr5LhNmouY%2fklVwosOnl6IDi8IcZvwYZNA%2bkoQYOse6WHpedKlJERJJHrHXT8bDbt7%2fMmWYP5%2btx73jarE0Os3ZloBtsJPJWVh7RPN3aj7NRTouqoRfqZnRJw0QVJy4LI2abIowwaR3qmaNveCg5D7DxWuHa15syT33js5F7dlEW1F8cmHFj9F9NYDMHUUgxJwFLrldmsMOEz8bPZeHY%2bt7JO74aMnRTwvsOXVoYF2h1Fpi%2fnjSmi2U3KQK46AzLGC4dXTRqpQOkExyqdowGOgs2AchV4nJrbUKDwVfB9%2fuVdJbBPnjB37lQAnvW%2fuBR3T9fRxINBUwaHnfi3VgL8S19tgc5WSuNesQgLlneeMeCxGtlkh2o%2f7o%2bjUFUaYHIRD2SspApl9Jc0mb8CaX%2fZ%2fJYc2Eu8RAp%2bB5mT40HgsbiIJ6SDyAlpDpq7dO%2buMGUnKpVdNKOn0VaZ7r19yX4GCIyGkqZg9%2fQKcMryFlIl4MGSl0VPc8zvN%2bLV9j0sw7qHkcO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</body>
	<?php } ?>
</html>