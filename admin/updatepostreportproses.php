<?php
include "connect.php";

    if($_SESSION['status']!="admin")
    {?>
        <script type="text/javascript">window.location.assign("loginadmin.php")</script>
    <?php
    }
    
    $iduser = $_GET['iduser'];
	$idreport = $_GET['idreport'];
	$idpolicy = $_GET['idpolicy'];
    if (empty($iduser)) {
        ?> 
        <script language="javascript">alert("Tidak ada iduser yang masuk");</script>
        <script>document.location.href='daftarkeluhan.php';</script> 
    <?php
    }

    date_default_timezone_set('Asia/Jakarta');

	$title = $_POST['postTitle'];
	$content = $_POST['postCont'];
	$date = date("Y-m-d H:i:s");
	
	$update = mysqli_query($conn,"UPDATE `policy` SET `idpolicy`='$idpolicy', `idreport`='$idreport', `iduser`='$iduser', `postTitle`='$title', `postCont`='$content', `postDate`='$date' WHERE idpolicy='$idpolicy'");
	if($update) {
		?>
		<script language="javascript">alert("Policy berhasil diupdate");</script>
		<script>document.location.href='daftarkebijakan.php';</script>
		<?php
	}
	else  {
		?>
		<script language="javascript">alert("Policy gagal diupdate, silahkan coba ulang");</script>
		<script>document.location.href='daftarkebijakan.php';</script>
		<?php
	}
	
?>