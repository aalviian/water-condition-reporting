<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
	
	<link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
				<li class="divider"></li>
				<li><a href="logoutproses.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="tambahwilayah.php"><i class="fa fa-bar-chart-o fa-fw"></i>Tambah Wilayah</a>
                        </li>
                        <li>
                            <a href="maps.php"><i class="fa fa-table fa-fw"></i>Maps</a>
                        </li>
                        <li>
                            <a href="listkelurahan.php"><i class="fa fa-edit fa-fw"></i>Daftar Keluhan</a>
                        </li>
                        <li>
                            <a href="listuser.php"><i class="fa fa-edit fa-fw"></i>Daftar User</a>
                        </li>
                        <li>
                            <a href="listwilayah.php"><i class="fa fa-edit fa-fw"></i>Daftar Wilayah</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
			
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Input Wilayah</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <form role="form">
									    <div class="form-group">
                                            <label>Choose Kota/Kab</label>
											<select class="form-control" id="color">                
												<option value=""> choose options </option>                
												<option value="1">Kota</option>
												<option value="2">Kabupaten</option>
											</select>
                                        </div>									
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <input class="form-control">
                                            <p class="help-block">Sesuai kecamatan yang ada di kota atau kabupaten Bogor</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Kelurahan</label>
                                            <input class="form-control">
                                            <p class="help-block">Sesuai kelurahan yang ada di kecamatan baik kota ataupun kab Bogor</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Latitude</label>
                                            <input class="form-control">
                                            <p class="help-block">Sesuai dengan kelurahan yang di inginkan</p>
                                        </div>
										<div class="form-group">
                                            <label>Longitude</label>
                                            <input class="form-control">
                                            <p class="help-block">Sesuai dengan kelurahan yang di inginkan</p>
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
								<div class="col-lg-8">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d507368.0597569395!2d106.53388971763698!3d-6.545285954570775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c3e312798437%3A0x301576d14feb990!2sBogor%2C+Jawa+Barat!5e0!3m2!1sid!2sid!4v1449759958350" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	
	<script type="text/javascript">
    $('#size').chainedTo('#color');
	</script>

</body>

</html>
