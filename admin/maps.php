<?php
include 'connect.php';
    if($_SESSION['status']!="admin")
    {?>
        <script type="text/javascript">window.location.assign("loginadmin.php")</script>
    <?php
    }

    if($_SESSION['status'] == "admin") {
    $id = $_SESSION['status'];
?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home | Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
</head>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM admin WHERE idadmin = '$id'");
    $result = mysqli_fetch_array($query);
  ?>
  
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hello, <?php echo $result['usernameadmin'] ?> !</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="divider"></li>
                <li><a href="logoutproses.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="active">
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="maps.php"><i class="fa fa-table fa-fw"></i>Maps</a>
                        </li>
                        <li>
                            <a href="daftarkeluhan.php"><i class="fa fa-edit fa-fw"></i>Daftar Keluhan</a>
                        </li>
						<li>
                            <a href="daftarkebijakan.php"><i class="fa fa-edit fa-fw"></i>Daftar Kebijakan</a>
                        </li>
                        <li>
                            <a href="daftaruser.php"><i class="fa fa-edit fa-fw"></i>Daftar User</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Maps</h1>
                </div>
            </div>
            <div class="row">

			 <br>
			 <div id="map"></div>
				<script>

					function initMap() {

                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: {lat: -6.596553, lng:  106.795665}
                  });

                    <?php 
                    $query=mysqli_query($conn, "SELECT * FROM report");
                    while ($idreport=mysqli_fetch_assoc($query)) { ?>

                      var myLatLng = {lat:<?php echo $idreport['lat']?>, lng:<?php echo $idreport['long']?>};

                      var marker = new google.maps.Marker({
                        position: myLatLng, 
                        map: map,
                        title: 'Drought Report!'
                      });

                      var infowindow = new google.maps.InfoWindow({
                      content: 
					  '<?php echo $idreport['kel']?>' + '<br>Latitude: ' + <?php echo $idreport['lat']?> + '<br>Longitude: ' + <?php echo $idreport['long']?> + '<br>Alamat lengkap: <?php echo $idreport['alamat']?>' + '<br>Waktu post: <?php echo $idreport['timestamp']?>'
                      });
					  
						infowindow.open(map, marker);

                  <?php } ?>
                 }

				  </script>

				<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA8aEnIxREfHqK8FTd3LOFQeP1tIlxI4U&signed_in=true&callback=initMap" type="text/javascript"></script>
				<br>
				<br>
				</div>
					
			</div>
              
         </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>
    <?php } ?>
</html>
<?php } ?>