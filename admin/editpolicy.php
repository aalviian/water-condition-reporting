<?php
  include 'connect.php';
  if($_SESSION['id']=="user"){
      ?>
      <script language="javascript">alert("Anda harus login terlebih dahulu");</script>
      <script>document.location.href='loginadmin.php';</script>
  <?php
  }
    $idreport = $_GET['idreport'];
	$iduser = $_GET['iduser'];
	$idpolicy = $_GET['idpolicy'];
    if (empty($idreport)) {
        ?> 
        <script language="javascript">alert("Tidak ada iduser yang masuk");</script>
        <script>document.location.href='daftarkeluhan.php';</script> 
    <?php
    }

 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home | Admin</title>
    
    <link rel="stylesheet" href="dist/css/normalize.css">
    <link rel="stylesheet" href="dist/css/main.css">
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     <script>
          tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
      </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

  <?php
    if($_SESSION['status']!="admin")
    {?>
        <script type="text/javascript">window.location.assign("loginadmin.php")</script>
    <?php
    }
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM admin WHERE idadmin = '$id'");
    $result = mysqli_fetch_array($query);
  ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hello, <?php echo $result['usernameadmin'] ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="divider"></li>
                <li><a href="logoutproses.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="active">
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="maps.php"><i class="fa fa-table fa-fw"></i>Maps</a>
                        </li>
                        <li>
                            <a href="daftarkeluhan.php"><i class="fa fa-edit fa-fw"></i>Daftar Keluhan</a>
                        </li>
						<li>
                            <a href="daftarkebijakan.php"><i class="fa fa-edit fa-fw"></i>Daftar Kebijakan</a>
                        </li>
                        <li>
                            <a href="daftaruser.php"><i class="fa fa-edit fa-fw"></i>Daftar User</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Post Report</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
                $queryku = mysqli_query($conn, "SELECT * FROM report where idreport='$idreport'");
                $resultku = mysqli_fetch_array($queryku);
				$query2ku = mysqli_query($conn, "SELECT * FROM user where iduser='$iduser'");
                $result2ku = mysqli_fetch_array($query2ku);
				$query3ku = mysqli_query($conn, "SELECT * FROM policy where idpolicy='$idpolicy'");
                $result3ku = mysqli_fetch_array($query3ku);
            ?>
            <div class="row">

                    <form action="updatepostreportproses.php?iduser="<?php echo $iduser?>"&idreport="<?php echo $idreport ?>"&idpolicy="<?php echo $idpolicy ?>" method='post'>
                        <p><label>Membalas report dari "<?php echo $result2ku['nameUser']; ?>". Lokasi di <?php echo $resultku['kel'];?>, <?php echo $resultku['kec'];?></label></p>
                        <p><label>Title</label><br />
                        <input type="text" name="postTitle" value="<?php echo $result3ku['postTitle']?>"></p>

                        <p><label>Content</label><br />
                        <textarea name="postCont" cols='60' rows='10' "></textarea></p>
                        <p><input type="submit" name="submit" value="Submit"></p>

                    </form>
                    <br>
                </div>
        </div>
    </div>


    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>
    <?php } ?>
</html>
