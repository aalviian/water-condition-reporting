<?php
include 'connect.php';
    if($_SESSION['status']!="admin")
    {?>
        <script type="text/javascript">window.location.assign("loginadmin.php")</script>
    <?php
    }

    if(isset($_SESSION['status'])) {
    $id = $_SESSION['status'];
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daftar Keluhan | Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM admin WHERE idadmin = '$id'");
    $result = mysqli_fetch_array($query);
  ?>
  
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hello, <?php echo $result['usernameadmin'] ?> !</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="divider"></li>
                <li><a href="logoutproses.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="active">
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="maps.php"><i class="fa fa-table fa-fw"></i>Maps</a>
                        </li>
                        <li>
                            <a href="daftarkeluhan.php"><i class="fa fa-edit fa-fw"></i>Daftar Keluhan</a>
                        </li>
						<li>
                            <a href="daftarkebijakan.php"><i class="fa fa-edit fa-fw"></i>Daftar Kebijakan</a>
                        </li>
                        <li>
                            <a href="daftaruser.php"><i class="fa fa-edit fa-fw"></i>Daftar User</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Keluhan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <style type="text/css">
                .tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                .tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
                .tftable tr {background-color:#d4e3e5;}
                .tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
                .tftable tr:hover {background-color:#ffffff;}
                </style>
               <table class="tftable" border="1">
                <tr>
                    <th>idreport</th>
                    <th>iduser</th>
                    <th>kotakab</th>
                    <th>kec</th>
                    <th>kel</th>
                    <th>alamat</th>
                    <th>lat</th>
                    <th>long</th>
					<th>time</th>
                    <th>Aksi</th>
                </tr>

                <?php
                    $qry = mysqli_query($conn,"SELECT * FROM report ORDER BY timestamp desc");
                    while ($keluhan = mysqli_fetch_assoc($qry)) {
                ?>

                <tr>
                    <td><?php echo $keluhan['idreport'];?> </td>
                    <td><?php echo $keluhan['iduser'];?></td>
                    <td><?php echo $keluhan['kotakab'];?></td>
                    <td><?php echo $keluhan['kec'];?></td>
                    <td><?php echo $keluhan['kel'];?></td>
                    <td><?php echo $keluhan['alamat'];?></td>
                    <td><?php echo $keluhan['lat'];?></td>
                    <td><?php echo $keluhan['long'];?></td>
					<td><?php echo $keluhan['timestamp'];?></td>
                    <td>
                        <a class="btn btn-primary" href="postreport.php?idreport=<?php echo $keluhan['idreport'];?>&iduser=<?php echo $keluhan['iduser'];?> ">Balas</a> | 
                        <a class="btn btn-danger" onClick="return warning();" href="deletereport.php?idreport=<?php echo $keluhan['idreport'];?>">Hapus</a>
                    </td>
                </tr>
                 <?php } ?>
                </table>
            </div>

        </div>

    <!-- jQuery -->
     <script>
    $(document).ready(function () {
        $('#home').addClass('active');
    });
    </script>

    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript">
		function warning() {
			return confirm('Apa anda yakin untuk menghapus report ini?');
		}
	</script>
	
</body>
    <?php } ?>

</html>
<?php } ?>