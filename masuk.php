<?php
  include 'connect.php';  
  require 'header.php';
?>
<div class="container">
  
    <div class="section">

      <div class="row">
        <center><h3> Form Login</h3></center>
            <div class="card-panel teal lighten-5 hoverable">
              <form action="loginproses.php" method="post">
                <div class="row center">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">account_circle</i>
                    <input type="text" name="email" id="email" />
                    <label for="email">Email</label>
                  </div>
                </div>                
                <div class="row center">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">vpn_key</i>
                      <input type="password" name="password" id="password"/>
                      <label for="password">Password</label>
                  </div>
                </div>
                <div class="row center">
                  <a href="ubahpassword.php">Lupa Password?</a>
                   <button class="btn waves-effect waves-teal" type="submit" value="submit" action='loginproses.php'>Submit
                      <i class="material-icons right">send</i>
                   </button>
                </div>
              </form>
            </div>
        </div>

    </div>
  </div>
  
<?php
  require 'footer.php';
?>