<?php
  include 'connect.php';
  if($_SESSION['id']=="user"){
      ?>
      <script language="javascript">alert("Anda harus login terlebih dahulu");</script>
      <script>document.location.href='masuk.php';</script>
  <?php
  }
?>

<!doctype html>
<html lang="en">

  <head>  	
  	<meta charset="utf-8">
    <meta name="description" content="Responsive Bootstrap Landing Page Template">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
		<title>Pluto - Material UI Inspired Bootstrap Template</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css" type="text/css" media="screen">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Include roboto.css to use the Roboto web font, material.css to include the theme and ripples.css to style the ripple effect -->
	<link href="css/material.min.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/materialize.min.css" type="text/css" rel="stylesheet"/>
  </head>

  <body>
  
    <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


    <div class="navbar navbar-invers menu-wrap">
      <div class="navbar-header text-center">
        <a class="navbar-brand logo-right" href="javascript:void(0)"><i img src="<?php echo $result['photoUser'] ?>" class="mdi-image-timelapse"></i>Pluto</a>
      </div>
        <ul class="nav navbar-nav main-navigation">
            <li><a href="home.php">Timeline</a></li>
            <li><a href="profile.php">Profile</a></li>
            <li class="active"><a href="post.php">Post</a></li>
            <li><a href="maps.php">Maps</a></li>
            <li><a href="inbox.php">Inbox</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Logout</a></li>
        </ul>
        <button class="close-button" id="close-button">Close Menu</button>
    </div>
  	
  	<div class="content-wrap">
      
      <div class="container">
          <div class="col-md-12">

            <div class="navbar navbar-inverse sticky-navigation navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="200">
              <div class="container">
                <div class="navbar-header">
                  <a class="logo-left " href="index.html"><i class="mdi-image-timelapse"></i>Airkita-Timeline</a>
                </div>
                <div class="navbar-right">
                  <button class="menu-icon"  id="open-button">
                    <i class="mdi-navigation-menu"></i>
                  </button>             
                </div>
              </div>
            </div>
        </div>   

	

	<br>
	
    <section id="features" class="section">
      <div class="container">
        <div class="row">
			<form class="col s6" action="postproses.php" method="POST" enctype="multipart/form-data">
			  <div class="row">
				<div class="input-field col s12">
				  <input id="kota" name="kota" type="text" class="validate">
				  <label for="kota">Kota/Kabupaten Bogor</label>
				</div>
				<div class="input-field col s6">
				  <input id="iduser" name="id" disabled value="<?php echo $id ?>"  type="text" >
				   <label for="disabled">Id User</label>
				</div>
			  </div>
			  <div class="row">
				<div class="input-field col s6">
				  <input id="kec" name="kec" type="text" class="validate">
				  <label for="kec">Kecamatan</label>
				</div>
				<div class="input-field col s6">
				  <input id="kel" name="kel" type="text" class="validate">
				  <label for="kel">Kelurahan</label>
				</div>
			  </div>
			  <div class="row">
				<div class="input-field col s12">
				  <input id="alamat" name="alamat" type="text" class="validate">
				  <label for="alamat">Alamat Lengkap</label>
				</div>
			  </div>
			  <div class="row">
				<div class="input-field col s6">
				  <input placeholder="" id="lat" name="lat" type="text" class="validate">
				  <label for="first_name">Latitude</label>
				</div>
				<div class="input-field col s6">
				  <input placeholder="" id="long" name="long" type="text" class="validate">
				  <label for="first_name">Longitude</label>
				</div>
			  </div>
			  <div class="container">
			  <div class="row">
				<button class="btn waves-effect waves-light" type="submit" name="action">Submit
				   <i class="material-icons">send</i>
			   </button>
			  </div>
			  </div>
			</form>
			
			<div class="col s6">
			  <div class = "row">
				<div>Untuk mengisi latitude dan longitude, silahkan select maps tempat anda</div>
			  <style type='text/css'>
				.eventtext {width:100%; margin-top:20px; font:10pt Arial; text-align:left; 
				  line-height:25px; background-color:#EDF4F8;padding:5px; border:1px dashed #C2DAE7;}
				#mapa {width:100%; height:340px; border:5px solid #DEEBF2;}
				ul {font:10pt arial; margin-left:25px; padding:0px;}
				li {margin-left:0px; padding:5px; list-style-type:decimal;}
				.small {font:9pt arial; color:gray; padding:2px; }
				</style>

				<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyAA8aEnIxREfHqK8FTd3LOFQeP1tIlxI4U" type="text/javascript"></script>



				<div id="mapa"></div>
				<div class="eventtext">
				<div>Lattitude : <span id="latspan"></span></div>
				<div>Longitude: <span id="lngspan"></span></div>
				<div>Lat Long  : <span id="latlong"></span></div>


				<script type="text/javascript">
				  if (GBrowserIsCompatible()) 
				  {
					map = new GMap2(document.getElementById("mapa"));
					map.addControl(new GLargeMapControl());
					map.addControl(new GMapTypeControl(3)); 
					map.setCenter( new GLatLng(-6.557040, 106.738082), 11,0);
					
					GEvent.addListener(map,'mousemove',function(point)
					{
					  document.getElementById('latspan').innerHTML = point.lat()
					  document.getElementById('lngspan').innerHTML = point.lng()
					  document.getElementById('latlong').innerHTML = point.lat() + ', ' + point.lng()           
					});
					
					GEvent.addListener(map,'click',function(overlay,point)
					{
					  document.getElementById('lat').value = point.lat()
					  document.getElementById('long').value = point.lng()
					});
				  }
				</script>

				<!-- by line -->

				  <script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=124948124275250";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				</div>

			  </div>
			</div>
        </div>
      </div>
    </section>  
		

	<script src="material.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/jquery.chained.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/jquery.mmenu.min.all.js"></script> 
    <script src="js/count-to.js"></script>   
    <script src="js/jquery.inview.min.js"></script>     
    <script src="js/main.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/jquery.nav.js"></script>      
    <script src="js/smooth-on-scroll.js"></script>
    <script src="js/smooth-scroll.js"></script>
    

    <script>
        $(document).ready(function() {
            // This command is used to initialize some elements and make them work properly
            $.material.init();
			$('.fixed-action-btn').openFAB();
			$('.fixed-action-btn').closeFAB();
        });
    </script>
	<?php } ?>
  </body>
	
</html>
