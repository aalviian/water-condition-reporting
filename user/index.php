<?php
  include 'connect.php';
  if($_SESSION['id']=="user"){
      ?>
      <script language="javascript">alert("Anda harus login terlebih dahulu");</script>
      <script>document.location.href='masuk.php';</script>
  <?php
  }
?>

<!doctype html>
<html lang="en">

  <head>  	
  	<meta charset="utf-8">
    <meta name="description" content="Responsive Bootstrap Landing Page Template">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
		<title>Pluto - Material UI Inspired Bootstrap Template</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css" type="text/css" media="screen">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Include roboto.css to use the Roboto web font, material.css to include the theme and ripples.css to style the ripple effect -->
	<link href="css/material.min.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/materialize.min.css" type="text/css" rel="stylesheet"/>
  </head>

  <body>
  
    <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


    <div class="navbar navbar-invers menu-wrap">
      <div class="navbar-header text-center">
        <a class="navbar-brand logo-right" href="javascript:void(0)"><i img src="<?php echo $result['photoUser'] ?>" class="mdi-image-timelapse"></i>Pluto</a>
      </div>
        <ul class="nav navbar-nav main-navigation">
            <li class="active"><a href="home.php">Timeline</a></li>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="post.php">Post</a></li>
            <li><a href="maps.php">Maps</a></li>
            <li><a href="inbox.php">Inbox</a></li>
            <li><a href="faquser.php">FAQ</a></li>
            <li><a href="logoutproses.php">Logout</a></li>
        </ul>
        <button class="close-button" id="close-button">Close Menu</button>
    </div>
  	
  	<div class="content-wrap">
      
      <div class="container">
          <div class="col-md-12">

            <div class="navbar navbar-inverse sticky-navigation navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="200">
              <div class="container">
                <div class="navbar-header">
                  <a class="logo-left " href="index.html"><i class="mdi-image-timelapse"></i>Airkita-Timeline</a>
                </div>
                <div class="navbar-right">
                  <button class="menu-icon"  id="open-button">
                    <i class="mdi-navigation-menu"></i>
                  </button>             
                </div>
              </div>
            </div>
        </div>   

	

	<br>
	
    <section id="features" class="section">
      <div class="container">
        <div class="row">
			<main class="mdl-layout__content mdl-color--grey-100">
				<div class="fixed-action-btn" style="bottom: 45px; right: 80px;">
				  <a class="btn-floating btn-large red">
					<i class="large material-icons">mode_edit</i>
				  </a>
				  <ul>
					<li><a  href="post.php" class="btn-floating red"><i class="material-icons">note_add</i></i></a></li>
					<li><a href="maps.php" class="btn-floating yellow darken-1"><i class="material-icons">business</i></a></li>
					<li><a class="btn-floating green"><i class="material-icons">perm_identity</i></a></li>
					<li><a class="btn-floating blue"><i class="material-icons">payment</i></a></li>
				  </ul>
				</div>
			</main>
        </div>
      </div>
    </section>  
		

	<script src="material.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/jquery.chained.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/jquery.mmenu.min.all.js"></script> 
    <script src="js/count-to.js"></script>   
    <script src="js/jquery.inview.min.js"></script>     
    <script src="js/main.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/jquery.nav.js"></script>      
    <script src="js/smooth-on-scroll.js"></script>
    <script src="js/smooth-scroll.js"></script>
    

    <script>
        $(document).ready(function() {
            // This command is used to initialize some elements and make them work properly
            $.material.init();
			$('.fixed-action-btn').openFAB();
			$('.fixed-action-btn').closeFAB();
        });
    </script>
	<?php } ?>
  </body>
	
</html>
