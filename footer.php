  <footer class="page-footer light-blue lighten-1">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Instituional Bio</h5>
          <p class="grey-text text-lighten-4">Kepala Dinas Binamarga dan Pengairan</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">AirKita</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="utama/js/jquery-2.1.4.min.js"></script>
  <script src="utama/js/materialize.min.js"></script>
  <script src="utama/js/init.js"></script>
  <script src="utama/js/jquery.chained.min.js"></script>
  <script>
    $(document).ready(function() {
      $(".button-collapse").sideNav();
      $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears:60 // Creates a dropdown of 15 years to control year
      });
      $('select').material_select();
      $('.slider').slider({full_width: true});
      $('.slider').slider('pause');
      $('.slider').slider('start');
    });
      $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
    });
      $('.fixed-action-btn').openFAB();
      $('.fixed-action-btn').closeFAB();
  </script>
  
  </body>
</html>
