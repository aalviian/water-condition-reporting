<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>AirKita</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><img src="Airkita.png"/><a id="logo-container" href="#" class="brand-logo"></a> 
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php">Home</a></li>
        <li><a href="tentang.php">Tentang Kita</a></li>
        <li><a href="faq.php">FAQ</a></li>
        <li><a href="masuk.php">Masuk</a></li>
        <li><a href="daftar.php">Daftar</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="index.php">Home</a></li>
        <li><a href="tentang.php">Tentang Kita</a></li>
        <li><a href="faq.php">FAQ</a></li>
        <li><a href="masuk.php">Masuk</a></li>
        <li><a href="daftar.php">Daftar</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>