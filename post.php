<?php
include 'connect.php';
    if($_SESSION['status'] != "user")
    {?>
        <script type="text/javascript">window.location.assign("masuk.php")</script>
    <?php
    }

    if(isset($_SESSION['status'])) {
    $id = $_SESSION['status'];
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Post</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="user/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="user/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="user/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="user/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="utama/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="utama/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  </head>
  <body>

  <?php
    if(isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = mysqli_query($conn, "SELECT * FROM user WHERE idUser = '$id'");
    $result = mysqli_fetch_array($query);
  ?>


  <nav  class="light-blue lighten-1" role="navigation">
      <div class="nav-wrapper">
        <a href="home.php" class="brand-logo"><i class="large material-icons">backspace</i>Back</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
  </nav>
<br>

<div class="container">
  <div class ="row">
     <h5>Form Laporan Kekeringan (Khusus warga Bogor)</h5>
    <form class="col s6" action="postproses.php" method="POST" enctype="multipart/form-data">
      <div class="row">
        <div class="input-field col s6">
          <input id="kota" name="kota" type="text" class="validate" required>
          <label for="kota">Kota/Kabupaten</label>
        </div>
        <div class="input-field col s6">
          <input id="iduser" name="id" disabled value="<?php echo $id ?>"  type="text" required>
           <label for="disabled">Id User</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input id="kec" name="kec" type="text" class="validate" required>
          <label for="kec">Kecamatan</label>
        </div>
        <div class="input-field col s6">
          <input id="kel" name="kel" type="text" class="validate" required>
          <label for="kel">Kelurahan</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="alamat" name="alamat" type="text" class="validate" required>
          <label for="alamat">Alamat Lengkap</label>
        </div>
      </div>
      <div class="row center">
          <div class="input-field col s12">
            <i class="material-icons prefix"></i>
              <div class="file-field input-field">
                <div class="btn">
                  <span>File</span>
                  <input type="file" name="photowilayah" required>
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" name="photowilayah" placeholder="Upload your photo" required>
                </div>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="" id="lat" name="lat" type="text" class="validate" required>
          <label for="first_name">Latitude</label>
        </div>
        <div class="input-field col s6">
          <input placeholder="" id="long" name="long" type="text" class="validate" required>
          <label for="first_name">Longitude</label>
        </div>
      </div>
	  <div class="row">
		<div class="input-field col s12">
		  <textarea id="textarea1" name="deskripsi" class="materialize-textarea" required></textarea>
		  <label for="textarea1">Deskripsi Laporan</label>
		</div>
	  </div>
      <div class="container">
      <div class="row">
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
           <i class="material-icons">send</i>
       </button>
      </div>
      </div>
    </form>

    <div class="col s6">
      <div class = "row">
        <div>Untuk mengisi latitude dan longitude, silahkan select maps tempat anda</div>
      <style type='text/css'>
        .eventtext {width:100%; margin-top:20px; font:10pt Arial; text-align:left; 
          line-height:25px; background-color:#EDF4F8;padding:5px; border:1px dashed #C2DAE7;}
        #mapa {width:100%; height:340px; border:5px solid #DEEBF2;}
        ul {font:10pt arial; margin-left:25px; padding:0px;}
        li {margin-left:0px; padding:5px; list-style-type:decimal;}
        .small {font:9pt arial; color:gray; padding:2px; }
       </style>

        <script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyAA8aEnIxREfHqK8FTd3LOFQeP1tIlxI4U" type="text/javascript"></script>



        <div id="mapa"></div>
        <div class="eventtext">
        <div>Lattitude : <span id="latspan"></span></div>
        <div>Longitude: <span id="lngspan"></span></div>
        <div>Lat Long  : <span id="latlong"></span></div>


        <script type="text/javascript">
          if (GBrowserIsCompatible()) 
          {
            map = new GMap2(document.getElementById("mapa"));
            map.addControl(new GLargeMapControl());
            map.addControl(new GMapTypeControl(3)); 
            map.setCenter( new GLatLng(-6.557040, 106.738082), 20,0);
            
            GEvent.addListener(map,'mousemove',function(point)
            {
              document.getElementById('latspan').innerHTML = point.lat()
              document.getElementById('lngspan').innerHTML = point.lng()
              document.getElementById('latlong').innerHTML = point.lat() + ', ' + point.lng()           
            });
            
            GEvent.addListener(map,'click',function(overlay,point)
            {
              document.getElementById('lat').value = point.lat()
              document.getElementById('long').value = point.lng()
            });
          }
        </script>

        <!-- by line -->

      </div>

    </div>
  </div>


    <script src="user/material.min.js"></script>
    <script src="utama/js/jquery-2.1.4.min.js"></script>
    <script src="utama/js/materialize.min.js"></script>
    <script src="utama/js/init.js"></script>
    <script src="utama/js/jquery.chained.min.js"></script>
    <script>
         $(".button-collapse").sideNav();
        $('.fixed-action-btn').openFAB();
        $('.fixed-action-btn').closeFAB();
    </script>

    <?php } ?>
  </body>
  </html>
  <?php } ?>